<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        
        <jsp:include page="componente.jsp">
            <jsp:param name="name" value="Componente 1"/>
        </jsp:include>
        
        <jsp:include page="componente.jsp">
            <jsp:param name="name" value="Componente 2"/>
        </jsp:include>
    </body>
</html>
